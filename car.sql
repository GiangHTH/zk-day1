-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3307
-- Thời gian đã tạo: Th10 27, 2017 lúc 03:24 PM
-- Phiên bản máy phục vụ: 10.1.26-MariaDB
-- Phiên bản PHP: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `car`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `info_car`
--

CREATE TABLE `info_car` (
  `id` int(11) NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `make` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preview` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `price` int(11) DEFAULT NULL,
  `desciption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `info_car`
--

INSERT INTO `info_car` (`id`, `model`, `make`, `preview`, `description`, `price`, `desciption`) VALUES
(1, 'Primera', 'Nissan', '/widgets/img/car1.png', 'The Nissan Primera was produced between 2002 and 2008. It was available as a 4-door sedan or a 5-door hatchback or estate. The entry-level 1.6-liter petrol feels underpowered for a large car. The 1.8-liter petrol is keen, the refined 2.0-liter unit is the star performer. An improved 2.2-liter turbodiesel performs well, but is only relatively economical, as it\'s competitors in this class with similar characteristics offer even lower fuel consumption.', 23320, NULL),
(2, 'Cefiro', 'Nissan', '/widgets/img/car2.png', 'The Nissan Cefiro is an intermediate-size automobile range sold in Japan and other countries. It was available only as a 4 door sedan. A large proportion were equipped with automatic transmissions. Originally marketed towards the Japanese salaryman the top model used the same engine as found in the R32 Nissan Skyline, a 2 litre turbo charged 6 cylinder engine capable of just over 200 hp (150 kW). Other variants came with other versions of the Nissan RB engine. Brand new, the Cefiro was slightly more expensive than the equivalent Nissan Skyline.', 38165, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `info_car`
--
ALTER TABLE `info_car`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `info_car`
--
ALTER TABLE `info_car`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
