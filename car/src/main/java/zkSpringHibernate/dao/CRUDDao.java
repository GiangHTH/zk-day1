/**
 * 
 */
package zkSpringHibernate.dao;

import java.util.List;

/**
 * @author Giang
 *
 */
public interface CRUDDao {
	<T> List<T> getAll(Class<T> klass);
	
}
