/**
 * 
 */
package zkSpringHibernate.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Giang
 *
 */
@Repository
public class CRUDDaoImpl implements CRUDDao {

	@Autowired
	SessionFactory sessionFactory;
	
	protected final Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	
	/* (non-Javadoc)
	 * @see zkSpringHibernate.dao.CRUDDao#getAll(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getAll(Class<T> klass) {
		return getCurrentSession().createQuery("from "+klass.getName()).list();
	}
}
