/**
 * 
 */
package zkSpringHibernate.service;

import java.util.List;

import zkSpringHibernate.domain.InfoCar;

/**
 * @author Giang
 *
 */
public interface CRUDService {
	<T> List<T> getAll(Class<T> klass);
	
	public List<InfoCar> search(String keyword);
}
