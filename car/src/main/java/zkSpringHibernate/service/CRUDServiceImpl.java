/**
 * 
 */
package zkSpringHibernate.service;

import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zkSpringHibernate.dao.CRUDDao;
import zkSpringHibernate.domain.InfoCar;


/**
 * @author Giang
 *
 */
@Service
public class CRUDServiceImpl implements CRUDService {

	@Autowired
	private CRUDDao crudDao;
	/* (non-Javadoc)
	 * @see zkSpringHibernate.service.CRUDService#getAll(java.lang.Class)
	 */
	@Override
	@Transactional
	public <T> List<T> getAll(Class<T> klass) {
		return crudDao.getAll(klass);
	}
	@Override
	public List<InfoCar> search(String keyword) {
		List<InfoCar> result = new LinkedList<InfoCar>();
		if (keyword == null || "".equals(keyword)) {
			result = getAll(InfoCar.class);
		} else {
			for (InfoCar infoCar : getAll(InfoCar.class)) {
				if (infoCar.getModel().toLowerCase().contains(keyword.toLowerCase()) 
						|| infoCar.getMake().toLowerCase().contains(keyword.toLowerCase())) {
					result.add(infoCar);
				}
			}
		}
		return result;
	}

}
