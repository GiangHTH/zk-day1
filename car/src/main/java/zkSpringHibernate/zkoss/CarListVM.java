/**
 * 
 */
package zkSpringHibernate.zkoss;

import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;

import zkSpringHibernate.domain.InfoCar;
import zkSpringHibernate.service.CRUDService;

/**
 * @author Giang
 *
 */
public class CarListVM {

	@WireVariable
	private CRUDService crudService;
	
	private String keyword;
	private List<InfoCar> listCar = null;
	private InfoCar selectedCar;
	
	
	public String getKeyword() {
		return keyword;
	}


	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}


	public List<InfoCar> getListCar() {
		return listCar;
	}


	public InfoCar getSelectedCar() {
		return selectedCar;
	}


	public void setSelectedCar(InfoCar selectedCar) {
		this.selectedCar = selectedCar;
	}

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		listCar = crudService.getAll(InfoCar.class);
	}
	
	@Command
	@NotifyChange("listCar")
	public void search(){
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		listCar = crudService.search(keyword);
	}
}
